# README

This is a somewhat simple demo Rails Free Software Melbourne application

GHub/GLab markdown cheat sheet included her for my own convenience

# The largest heading
## The second largest heading
###### The smallest heading

**This is bold text**

*This text is italicized*

~~This was mistaken text~~

**This text is _extremely_ important**

quote text with a >:

> Pardon my French

Use `backticks` to highlight code

tripple backticks:
```
echo make
echo larger blocks
```

Links use [square-bracket-round-bracket](https://puzzleduck.org/)

- unordered lists
- use dashes -
- or asterix *

1. ordered lists
2. are numbered
  1. nest with
  2. two spaces
    1. as needed
    2. if needed
3. both
  * ordered and
  * unordered

- [ ] task lists
- [ ] are unordered lists
- [x] followed with [ ] or [x]
  - [x] and sub tasks
  - [ ] work like sub lists

shipit squirrel :shipit: lives here :+1:

\* backslash ignores
\* markdown formatting

<table>
    <tr>
        <td>Tables!</td>
    </tr>
</table>

Username support should link to me: @puzzleduck
