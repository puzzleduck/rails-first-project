# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


User.create!([
    {name: "Firsty (numero 1) McFirston", email: "example@railstutorial.org", password: "password", password_confirmation: "password"},
    {name: "Toot Toot 2oosie", email: "puzzleduck@gmail.com", password: "password", password_confirmation: "password", admin: "true"},
    {name: "3orMore", email: "3@unique.com", password: "password", password_confirmation: "password"},
    {name: "4444", email: "4@unique.com", password: "password", password_confirmation: "password"},
    {name: "5555", email: "5@unique.com", password: "password", password_confirmation: "password"},
    {name: "One 6ey beast", email: "6@unique.com", password: "password", password_confirmation: "password"},
  ])

40.times do |n|
  name = Faker::Name.name
  email = Faker::Internet.email
  pw = "Passw0rd"
  User.create!(name: name, email: email, password: pw, password_confirmation: pw)
end

UserFile.create!([
  { file_name: "test.txt", content: "just\nsome\n\ntext", user_id: 1 }
  ])

50.times do |n|
  UserFile.create!([
    { file_name: Faker::Lorem.characters(11), content: Faker::Company.bs, user_id: 1 }
    ])
end

50.times do |n|
  UserFile.create!([
    { file_name: Faker::Lorem.characters(11), content: Faker::Hacker.say_something_smart, user_id: 1 }
    ])
end
