class CreateUserFiles < ActiveRecord::Migration[5.0]
  def change
    create_table :user_files do |t|
      t.string :file_name
      t.text :content
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :user_files, [:user_id, :created_at]
  end
end
