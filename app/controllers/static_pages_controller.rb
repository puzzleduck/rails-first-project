class StaticPagesController < ApplicationController

@@count = 0

  def home
    @@count += 1
    if loggedin?
      @user_file = current_user.user_files.build
      @file_listing = current_user.user_files.paginate(page: params[:page])
    end
  end

  def help
  end

  def about
  end
end
