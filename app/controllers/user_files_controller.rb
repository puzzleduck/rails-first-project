class UserFilesController < ApplicationController
  before_action :loggedin_user, only: [:create, :destroy]

  def create
    @user_file = current_user.user_files.build(user_file_params)
    if @user_file.save
      flash[:success] = "File generated"
      redirect_to root_path
    else
      @file_listing = []
      render "static_pages/home"
    end
  end

  def destroy
  end

  private

    def user_file_params
      params.require(:user_file).permit(:content)
    end

end
