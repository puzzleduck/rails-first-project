module ApplicationHelper

  def title_helper(page_title = "")
    if page_title.empty?
      ":: RailsDucK ::"
    else
      ":: " + page_title + " ::"
    end
  end

end
