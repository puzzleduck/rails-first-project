class UserFile < ApplicationRecord
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :file_name, presence: true, length: { maximum: 11 }
  validates :content, presence: true, length: { maximum: 255 }
end
