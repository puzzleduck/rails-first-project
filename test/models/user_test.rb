require 'test_helper'

class UserTest < ActiveSupport::TestCase

  def setup
    @user = User.new(name: "3Ln", email: "e@m.c", password: "password", password_confirmation: "password")
  end

  test "simple user is valid" do
    assert @user.valid?
  end

  test "name should not be blank" do
    @user.name = "  \n"
    assert_not @user.valid?
  end

  test "email should not be blank" do
    @user.email = "  \n"
    assert_not @user.valid?
  end

  test "name should be longer than 2 chars" do
    @user.name = "an"
    assert_not @user.valid?
  end

  test "name should be shorter than 101 chars" do
    @user.name = "a" * 101
    assert_not @user.valid?
  end

  test "valid emails should be accepted" do
    valid_emails = %w[simple@dot.com MIX.cap@SS.s un_der@da-sh.co multi+part@both.sides.co.uk.gov]
    valid_emails.each do |valid_email|
      @user.email = valid_email
      assert @user.valid?, "the address <<#{valid_email.inspect}>> should be valid"
    end
  end

  test "invalid emails should NOT be accepted" do
    valid_emails = %w[no@dot,com NO.at.SS double@at@at.co wrong@multi+part.com double@dots..com]
    valid_emails.each do |valid_email|
      @user.email = valid_email
      assert_not @user.valid?, "the address <<#{valid_email.inspect}>> should not be valid"
    end
  end

  test "email address should be unnique" do
    clone = @user.dup
    clone.email.upcase!
    @user.save
    assert_not clone.valid?
  end

  test "email stored in lowercase" do
    upcase_email = "UP@CASE.COM"
    @user.email = upcase_email
    @user.save
    assert_equal upcase_email.downcase, @user.reload.email
  end

  test "password should not be blank" do
    @user.password = @user.password_confirmation = "                "
    assert_not @user.valid?
  end

  test "password should not be shorter than 6 chars" do
    @user.password = @user.password_confirmation = "Not6c"
    assert_not @user.valid?
  end


end
