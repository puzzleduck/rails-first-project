require 'test_helper'

class UserFileTest < ActiveSupport::TestCase

  def setup
    @user = users(:firsty)
    @user_file = user_files(:first)
  end

  test "default file valid?" do
    assert @user.user_files.create!(file_name: "test.txt", content: "plane\nold\n\ntext").valid?
  end

  test "file without id invalid?" do
    @user_file.user_id = nil
    assert_not @user_file.valid?
  end

  test "contents longer than 255 invalid?" do
    @user_file.content = "A"*256
    assert_not @user_file.valid?
  end

  test "name longer than 11 invalid?" do
    @user_file.file_name = "A"*12
    assert_not @user_file.valid?
  end

  test "most recent file should be first" do
    assert_equal user_files(:latest), UserFile.first
  end

  test "deleted user should have files deleted" do
    @temp = User.new(name: "3Ln", email: "e@m.c", password: "password", password_confirmation: "password")
    @temp.save
    @file = @temp.user_files.create!(file_name: "test.txt", content: "plane\nold\n\ntext")
    assert_difference 'UserFile.count', -1 do
      @temp.destroy
    end
  end

end
