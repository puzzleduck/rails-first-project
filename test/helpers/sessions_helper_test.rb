require "test_helper"

class SessionsHelperTest < ActionView::TestCase

  def setup
    @user = users(:firsty)
    remember(@user)
  end

  test "correct user in case of nil session" do
    assert_equal @user, current_user
    assert is_loggedin?
  end

  test "incorrect digest results in nil user" do
    @user.update_attribute(:remember_digest, User.digest(User.new_token))
    assert_nil current_user
  end
end
