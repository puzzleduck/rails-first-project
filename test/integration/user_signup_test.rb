require 'test_helper'

class UserSignupTest < ActionDispatch::IntegrationTest

  test "signup works" do
    get signup_path
    assert_difference 'User.count', 1 do
      post users_path, params: { user: { name: "123", email: "a@b.com", password: "Same_same", password_confirmation: "Same_same" } }
    end
    follow_redirect!
    assert_template "users/show"
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path(User.find_by(email: "a@b.com"))
    assert is_loggedin?
  end

  test "signup fails on wrong password" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, params: { user: { name: "123", email: "a@b.com", password: "same_same", password_confirmation: "not_same" } }
    end
    assert_template "users/new"
    assert_select "div#error_explanation"
    assert_select "div.alert-danger"
    assert_not is_loggedin?
  end

  test "signup fails on invalid name" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, params: { user: { name: "", email: "a@b.com", password: "same_same", password_confirmation: "same_same" } }
    end
    assert_template "users/new"
    assert_select "div#error_explanation"
    assert_select "div.alert-danger"
    assert_not is_loggedin?
  end

  test "signup fails on invalid email" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, params: { user: { name: "123", email: "@b.com", password: "same_same", password_confirmation: "same_same" } }
    end
    assert_template "users/new"
    assert_select "div#error_explanation"
    assert_select "div.alert-danger"
    assert_not is_loggedin?
  end

end
