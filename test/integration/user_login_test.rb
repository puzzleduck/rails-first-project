require 'test_helper'

class UserLoginTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:firsty)
  end

  test "failed login should display error for one view" do
    get login_path
    assert_template "sessions/new"
    post login_path, params: { session: { email: "", password: "" } }
    assert_template "sessions/new"
    assert_not flash.empty?
    assert_not is_loggedin?
    get root_path
    assert flash.empty?
  end

  test "valid login should work, and allow logout" do
    get login_path
    post login_path, params: { session: { email: @user.email, password: "password" } }
    assert_redirected_to @user
    follow_redirect!
    assert_template "users/show"
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path(@user)
    assert is_loggedin?
    delete logout_path
    assert_not is_loggedin?
    assert_redirected_to root_path
    delete logout_path #simulate second window logout
    follow_redirect!
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path, count: 0
    assert_select "a[href=?]", user_path(@user), count: 0
  end

  test "nil digest should return false" do
    assert_not @user.authenticated?("")
  end

  test "login without remember_me should forget" do
    login_as(@user, remember_me: "0")
    assert_nil cookies['remember_token']
  end

  test "login with remember_me should remember" do
    login_as(@user, remember_me: "1")
    assert_not_nil cookies['remember_token']
  end

end
