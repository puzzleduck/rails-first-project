require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
  include ApplicationHelper

  def setup
    @user = users(:firsty)
  end

  test "profile should display correctly" do
    get user_path(@user)
    assert_template "users/show"
#    assert_select "title", full_title(@user.name)
    assert_select "h2", text: @user.name
    assert_select "h2>img.gravatar"
    assert_match @user.user_files.count.to_s, response.body
    assert_select "div.pagination"
    @user.user_files.paginate(page: 1).each do |file|
      assert_match file.content, response.body
    end
  end

end
