require 'test_helper'



class UsersControllerTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:firsty)
  end


  test "should not be able to make users admin" do
    login_as(@user)
    assert_not @user.admin?
    patch user_path(@user), params: { user: { password: "", password_confirmation: "", admin: 1 } }
    assert_not @user.reload.admin?
  end

end
