require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:firsty)
    @other_user = users(:two)
  end

  test "attempt to edit followed by login should go to edit page" do
    get edit_user_path(@user)
    login_as(@user)
    assert_redirected_to edit_user_path(@user)
    name = "Other Edit"
    email = "puzzleduck@puzzleduck.org"
    patch user_path(@user), params: { user: { name: name, email: email, password: "", password_confirmation: "" } }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal name, @user.name
    assert_equal email, @user.email
  end

  test "attempt to edit followed by login-logout-login go to user page" do
    get edit_user_path(@user)
    assert_not_nil session[:forwarding_url]
    login_as(@user)
    assert_redirected_to edit_user_path(@user)
    delete logout_path
    get root_path
    assert_nil session[:forwarding_url]
    login_as(@user)
    assert_nil session[:forwarding_url]
  end

  test "unsucessfull edit displays errors and redirects correctly" do
    login_as(@user)
    get edit_user_path(@user)
    assert_template "users/edit"
    patch user_path(@user), params: { user: { name: "", email: "fail@", password: "not", password_confirmation: "matching" } }
    assert_template "users/edit"
    assert_select "div.alert-danger", "submission contains 5 errors"
    get root_path
    assert flash.empty?
  end

  test "sucessfull edit displays updated user correctly" do
    login_as(@user)
    get edit_user_path(@user)
    assert_template "users/edit"
    name = "First Edit"
    email = "puzzleduck@gmail.com"
    pw = "Upda7ed"
    patch user_path(@user), params: { user: { name: name, email: email, password: pw, password_confirmation: pw } }
    assert_redirected_to @user
    @user.reload
    follow_redirect!
    assert_equal name, @user.name
    assert_equal email, @user.email
    assert_template "users/show"
    assert_select "div.alert-danger", count: 0
  end

  test "user should not be able to edit other users profiles" do
    login_as(@other_user)
    get edit_user_path(@user)
    assert flash.empty?
    assert_redirected_to root_path
  end

  test "redirects and errors for unloggedin user changing profile 1" do
    get edit_user_path(@user)
    assert_not flash.empty?
    assert_redirected_to login_path
  end

  test "user should not be able to patch other users profiles" do
    login_as(@user)
    patch user_path(@other_user), params: { user: { name: @user.name, email: @user.email } }
    assert flash.empty?
    assert_redirected_to root_path
  end

end
