require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:firsty)
    @other_user = users(:two)
    @alt_user = User.new(name: "3Ln", email: "e@m.c", password: "password", password_confirmation: "password")
  end

  test "should get signup page" do
    get signup_path
    assert_response :success
  end

  test "redirects and errors for unloggedin user changing profile 2" do
    patch user_path(@user), params: { user: { name: @user.name, email: @user.email } }
    assert_not flash.empty?
    assert_redirected_to login_path
  end

  test "non-member should not see user list" do
    get users_path
    assert_redirected_to login_path
  end

   test "should redirect for edit on wrong user" do
     login_as(@user)
#     get edit_user_path(@alt_user)
     assert flash.empty?
  #   assert_redirected_to root_path
   end

   test "should redirect for update on wrong user" do
     login_as(@user)
     patch user_path(@user), params: { user: { name: "notme", email: "dont@call.me" } }
#     assert flash.empty?
#     assert_redirected_to root_path
   end

end
