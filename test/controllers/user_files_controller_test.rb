require 'test_helper'

class UserFilesControllerTest < ActionDispatch::IntegrationTest

  def setup
    @user_file = user_files(:latest)
  end

  test "create file without login redirects" do
    assert_no_difference 'UserFile.count' do
      post user_files_path, params: { user_file: { file_name: "sometxx", content: "FAILED TEST" } }
    end
    assert_redirected_to login_path
  end

  test "destroy file without login redirects" do
    assert_no_difference 'UserFile.count' do
      delete user_file_path(@user_file)
    end
    assert_redirected_to login_path
  end

end
